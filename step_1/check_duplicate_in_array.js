
function check_duplicate_in_array(array) {
	var sorted_array = array.slice().sort();

	var return_result = [];

	for (var index = 0; index < sorted_array.length - 1; index++) {
		if (sorted_array[index+1] == sorted_array[index+1]) {
			return_result.push(sorted_array[index]);
		}
	}
	return return_result;
}

/** example (usage) */

var array = [1,2,3,4,5,6,7,5,8,9,0];
check_duplicate_in_array(array);